#! /bin/sh

# Use nss_wrapper to add the current user to /etc/passwd
# and enable the use of tools like git
USER_ID=$(id -u)
GROUP_ID=$(id -g)
# Pointless if running as root
if [[ "${USER_ID}" != '0' ]]; then
  export NSS_WRAPPER_PASSWD=/tmp/nss_passwd
  export NSS_WRAPPER_GROUP=/tmp/nss_group
  cp /etc/passwd $NSS_WRAPPER_PASSWD
  cp /etc/group  $NSS_WRAPPER_GROUP

  if ! getent passwd "${USER_ID}" >/dev/null; then
     # we need an entry in passwd for current user. Make sure there is no conflict
     sed -e '/^rdeck:/d' -i $NSS_WRAPPER_PASSWD
     mkdir  -p ${RDECK_BASE}/home
     echo "rdeck:x:${USER_ID}:${GROUP_ID}:Rundeck instance:${RDECK_BASE}/home:/sbin/nologin" >> $NSS_WRAPPER_PASSWD
  fi

  export LD_PRELOAD=libnss_wrapper.so
fi

if [[ -z "$HOSTNAME_FQDN" ]]; then
  export HOSTNAME_FQDN="https://${NAMESPACE}.web.cern.ch"
fi

# Copy all the custom configuration injected in folder /rundeck-config into
# $RDECK_BASE/server/config/ . If there are no files or they are empty, nothing
# is copied
if [ -n "$(ls -A /rundeck-config)" ]
then
  for f in /rundeck-config/*
  do
    if [ -s $f ]
    then
      envsubst < $f > $RDECK_CONFIG/`basename $f`
    fi
  done
fi

# Remove auth constraint from WEB-INF to run in preauthenticated mode
xmlstarlet ed -L -N x="http://java.sun.com/xml/ns/javaee" -d '//x:auth-constraint' /var/lib/rundeck/exp/webapp/WEB-INF/web.xml

source /etc/rundeck/profile
exec $rundeckd
